#! /bin/bash

make

# Generar set de dato chico para desarrollo
{ echo 2 & ./generator 2 10 ; } > 2d/db.txt
./generator 2 10 > 2d/input.txt

# Generar datos entre 3 y 5 dimensiones
# Se generan 5000 coordenadas por dimension y 20000 consultas por dimension

DB_SIZE=5000
QUERY_SIZE=2000

for dim in $(seq 3 5)
do
    { echo $dim & ./generator $dim $DB_SIZE ; } > ${dim}d/db.txt
    ./generator $dim $QUERY_SIZE > ${dim}d/input.txt
done

dim=3
SIZE=10
INVALID_SIZE=5
# Sin registros
echo $dim > no_data/db.txt
./generator $dim $SIZE > no_data/input.txt

# Solo un registro
{ echo $dim & ./generator $dim 1 ; } > one_data/db.txt
./generator $dim $SIZE > one_data/input.txt

# Datos con registros de mas dimensiones.
echo $dim > more_data_dimensions/db.txt
{ ./generator $dim $SIZE & ./generator 4 $INVALID_SIZE ; } | sort >> more_data_dimensions/db.txt
./generator $dim $SIZE > more_data_dimensions/input.txt

# Datos con registros de menos dimensiones.
echo $dim > less_data_dimensions/db.txt
{ ./generator $dim $SIZE & ./generator 2 $INVALID_SIZE ; } | sort >> less_data_dimensions/db.txt
./generator $dim $SIZE > less_data_dimensions/input.txt

# Consultas con registros de mas dimensiones.
{ echo $dim & ./generator $dim $SIZE ; } > more_query_dimensions/db.txt
{ ./generator $dim $SIZE & ./generator 4 $INVALID_SIZE ; } | sort > more_query_dimensions/input.txt

# Consultas con registros de menos dimensiones.
{ echo $dim & ./generator $dim $SIZE ; } > less_query_dimensions/db.txt
{ ./generator $dim $SIZE & ./generator 2 $INVALID_SIZE ; } | sort > less_query_dimensions/input.txt
