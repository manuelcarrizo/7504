#include <iostream>
#include <random>

/*
Codigo del generador de numeros al azar de http://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
*/

int FLOOR = 0;
int CEIL = 10000;

void usage()
{
    std::cout << "Uso: ./generator <D> <N> [<m>] [<M>]" << std::endl \
        << "Genera N registros de D dimensiones. Con valores entre m y M. " \
        << "Si no se especifican m y M sus valores son " << FLOOR << " y " << CEIL << std::endl;
    exit(0);
}

int main(int argc, char * argv[])
{
    if(argc != 3 && argc != 5)
        usage();

    int dimensions = atoi(argv[1]);
    int count = atoi(argv[2]);
    int floor = FLOOR;
    int ceil = CEIL;

    if(argc == 5) {
        floor = atoi(argv[3]);
        ceil = atoi(argv[4]);
    }

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(floor, ceil);

    for(int n = 0 ; n < count ; n++) {
        for(int d = 0; d < dimensions ; d++) {
            std::cout << dis(gen);
            if(d < (dimensions - 1))
              std::cout << ' ';
        }
        std::cout << "\n";
    }

    return 0;
}
