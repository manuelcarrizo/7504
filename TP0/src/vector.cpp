#include <cmath>

#include "vector.h"

Vector::Vector(int dim)
{
    dimension_ = dim;
    coords_ = new float[dimension_];
}

Vector::Vector(const Vector & other)
{
    dimension_ = other.dimension_;
    coords_ = new float[dimension_];

    for(int i = 0 ; i < dimension_ ; i++) {
        coords_[i] = other.coords_[i];
    }
}

Vector::~Vector(){
    delete[] coords_;
}

float & Vector::operator[](int index)
{
    if(index < dimension_)
        return coords_[index];

}

float Vector::distance(const Vector & other)
{
    return sqrt(square_distance(other));
}

float Vector::square_distance(const Vector & other)
{
    float ret = 0;
    for(int i = 0; i < dimension_ ; i++) {
        float diff = coords_[i] - other.coords_[i];

        ret += diff * diff;
    }
    return ret;
}

std::ostream& operator<< (std::ostream& stream, const Vector & vector)
{
    for(int i = 0; i < vector.dimension_ ; i++) {
        stream << vector.coords_[i] << ' ';
    }

    return stream;
}
