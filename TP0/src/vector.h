#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <iostream>

class Vector
{
private:
    int dimension_;
    float * coords_;

public:
    Vector(int dim);
    Vector(const Vector & other);
    ~Vector();

    int dimension() { return dimension_; };

    float & operator[](int index);

    float distance(const Vector & other);
    float square_distance(const Vector & other);

    friend std::ostream& operator<< (std::ostream& stream, const Vector & vector);
};

#endif // _VECTOR_H_
