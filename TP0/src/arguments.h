#ifndef _ARGUMENTS_H_
#define _ARGUMENTS_H_

#include <iostream>

class Arguments
{
public:
    ~Arguments();

    void parse(int argc, char *argv[]);

    static void usage();

    std::istream & points();
    std::istream & input();
    std::ostream & output();
};

#endif // _ARGUMENTS_H_
