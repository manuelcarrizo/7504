#include <fstream>

#include "arguments.h"
#include "cmdline.h"

using namespace std;

static istream *pss = 0;
static istream *iss = 0;
static ostream *oss = 0;
static fstream pfs;
static fstream ifs;
static fstream ofs;


static void opt_points(string const &);
static void opt_input(string const &);
static void opt_output(string const &);
static void opt_help(string const &);


Arguments::~Arguments()
{
    if(pfs.is_open())
        pfs.close();

    if(ofs.is_open())
        ofs.close();
}

void Arguments::usage()
{
    std::cout << "Uso:" << std::endl \
        << "./tp0 [-h] -p <points_db> [-i <input>] [-o <output>]" << std::endl \
        << "<input> archivo que contiene las consultas, " \
        << "puede especificarse '-' para leer de stdin" << std::endl \
        << "<output> archivo donde se escribiran los resultados, " \
        << "puede especificarse '-' para escribir a stdout" << std::endl \
        << "-h mostrar esta ayuda" << std::endl;
}

std::istream & Arguments::points()
{
    return *pss;
}

std::istream & Arguments::input()
{
    return *iss;
}

std::ostream & Arguments::output()
{
    return *oss;
}

void Arguments::parse(int argc, char *argv[])
{
    static option_t options[] = {
        {1, "p", "points", NULL, opt_points, OPT_MANDATORY},
        {1, "i", "input", "-", opt_input, OPT_DEFAULT},
        {1, "o", "output", "-", opt_output, OPT_DEFAULT},
        {0, "h", "help", NULL, opt_help, OPT_DEFAULT},
        {0, }
    };

    cmdline cmd(options);
    cmd.parse(argc, argv);
}


static void opt_points(string const & arg)
{
    pfs.open(arg.c_str(), ios::in);
    pss = &pfs;

    if (!pss->good()) {
        cerr << "cannot open "
                 << arg
                 << "."
                 << endl;
        exit(-1);
    }
}

static void opt_input(string const & arg)
{
    if (arg == "-") {
        iss = &cin;
    }
    else {
        ifs.open(arg.c_str(), ios::in);
        iss = &ifs;
    }

    if (!iss->good()) {
        cerr << "cannot open "
                 << arg
                 << "."
                 << endl;
        exit(-2);
    }
}

static void opt_output(string const & arg)
{
    if (arg == "-") {
        oss = &cout;
    } else {
        ofs.open(arg.c_str(), ios::out);
        oss = &ofs;
    }

    if (!oss->good()) {
        cerr << "cannot open "
             << arg
             << "."
            << endl;
        exit(-3);
    }
}

static void opt_help(string const &)
{
    Arguments::usage();
    exit(0);
}
