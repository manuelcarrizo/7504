#include <ios>
#include <iostream>
#include <fstream>
#include <limits>
#include <list>

#include "arguments.h"
#include "vector.h"

int load_vectors(int dimension, std::istream & input, std::list<Vector *> & list);
Vector * find_closest(std::list<Vector *> & list, Vector * query);

int main(int argc, char *argv[])
{
    Arguments args;
    args.parse(argc, argv);

    int dim = 0;
    args.points() >> dim;

    std::list<Vector *> points, input;

    load_vectors(dim, args.points(), points);
    load_vectors(dim, args.input(), input);

    std::list<Vector *>::iterator it;
    for(it = input.begin(); it != input.end(); it++) {
        Vector * c = find_closest(points, *it);

        if(c) {
            args.output() << *c << std::endl;
        }
        else {
            args.output() << std::endl;
            std::cerr << "ERROR Closest to " << **it << " not found" << std::endl;
        }
    }

    return 0;
}

int load_vectors(int dimension, std::istream & input, std::list<Vector *> & list)
{
    while(input) {
        Vector v(dimension);
        int index;
        for(index = 0 ; index < dimension ; index++) {
            float value = 0;
            input >> value;

            // Si no pude leer un float llegue al final de la linea
            bool eol = (input.rdstate() & std::ifstream::failbit) != 0;

            if(eol) break;

            v[index] = value;
        }

        if(index) {
            // Si no me encontre con un error al empezar a leer
            if(index != dimension) {
                std::cerr << "ERROR Read only " << index << " values" << std::endl;
            }

            list.push_back(new Vector(v));
        }

        // Ignoro todos los valores que siguen hasta el final de la linea
        input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    return list.size();
}

Vector * find_closest(std::list<Vector *> & list, Vector * query)
{
    float closest_distance = std::numeric_limits<float>::max();
    Vector * closest = NULL;

    std::list<Vector *>::iterator it;
    for(it = list.begin(); it != list.end(); it++) {
        Vector * item =  *it;
        float distance = item->distance(*query);

        if(distance <= closest_distance) {
            closest = item;
            closest_distance = distance;
        }
    }

    return closest;
}
