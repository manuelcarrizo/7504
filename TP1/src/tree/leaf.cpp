#include "leaf.h"
#include <limits>

Leaf::Leaf(VectorList & points, int depth, Component * left, Component * right)
    : Component(depth, this, left, right), points_(points)
{
}

Vector * Leaf::closest(Vector * other)
{
    return closest(*other);
}

Vector * Leaf::closest(Vector & other)
{
    Vector * item = NULL;
    float closest_distance = std::numeric_limits<float>::max();

    VectorIterator it;
    for(it = points_.begin() ; it != points_.end() ; it++) {
        float distance = (*it)->square_distance(other);

        if(distance < closest_distance) {
            item = *it;
            closest_distance = distance;
        }
    }

    return item;
}

void Leaf::print(std::ostream & stream) const
{
    stream << "Leaf with " << points_.size() << " items: ";

    VectorConstIterator it;
    for(it = points_.begin() ; it != points_.end() ; it++) {
        stream << "(" << (**it) << ") ";
    }

    stream << std::endl;
}
