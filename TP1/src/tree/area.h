#ifndef _AREA_H_
#define _AREA_H_

#include "vector.h"

class Area
{
public:
    Area();

    void update(Vector * point);
    void update(Vector & point);

    void reset();    

    Vector & min();
    Vector & max();

    Vector & min() const;
    Vector & max() const;

    bool contains(Vector * point);
    bool contains(Vector & point);

    float distance(Vector * vector);
    float distance(Vector & vector);

    float distance2(Vector * point);
    float distance2(Vector & point);

private:
    Vector * min_;
    Vector * max_;

    bool set_up_;
    
    void init(Vector & point);
};

#endif // _AREA_H_