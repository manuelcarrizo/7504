#include "tree.h"
#include "node.h"
#include "leaf.h"

static const int MAX_SIZE = 2;

Component * mktree(VectorList & points, Splitting & rule, int dimensions, int depth)
{
    if(points.size() <= MAX_SIZE) {
        return new Leaf(points, depth, NULL, NULL);
    }

    int axis = depth % dimensions;
    rule.axis(axis);
    float pivot = rule.value(points);

    VectorList left, right;

    VectorIterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        if((**it)[axis] <= pivot) {
            left.push_back(*it);
        }
        else {
            right.push_back(*it);
        }
    }

    return new Node(points, pivot, axis, depth,
        mktree(left, rule, dimensions, depth + 1),
        mktree(right, rule, dimensions, depth + 1)
    );    
}
