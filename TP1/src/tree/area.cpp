#include "area.h"

#include <cmath>
#include <algorithm>

Area::Area() : min_(NULL), max_(NULL), set_up_(false)
{
}

Vector & Area::min()
{
    return *min_;
}

Vector & Area::max()
{
    return *max_;
}

Vector & Area::min() const
{
    return *min_;
}

Vector & Area::max() const
{
    return *max_;
}

void Area::update(Vector * point)
{
    update(*point);
}

void Area::update(Vector & point)
{
    if(!set_up_)
        init(point);

    for(int i = 0 ; i < point.dimension() ; i++) {
        if( point[i] < min()[i] ) {
            min()[i] = point[i];
        }

        if( point[i] > max()[i] ) {
            max()[i] = point[i];
        }
    }
}

void Area::reset()
{
    if(min_) {
        delete min_;
        min_ = NULL;
    }
    if(max_) {
        delete max_;
        max_ = NULL;
    }

    set_up_ = false;
}

bool Area::contains(Vector * point)
{
    return contains(*point);
}

bool Area::contains(Vector & point)
{
    if(!min_ && !max_) return false;

    for(int i = 0 ; i < point.dimension() ; i++) {
        if(point[i] < min()[i] || point[i] > max()[i])
            return false;
    }

    return true;
}

void Area::init(Vector & point)
{
    min_ = new Vector(point);
    max_ = new Vector(point);

    set_up_ = true;
}

float Area::distance(Vector * point)
{
    return sqrt(distance2(point));
}

float Area::distance(Vector & point)
{
    return sqrt(distance2(point));
}

float Area::distance2(Vector * point)
{
    return distance2(*point);
}

float Area::distance2(Vector & point)
{
    float distance = 0;
    for(int i = 0; i < point.dimension() ; i++) {
        float start = min()[i];
        float end = max()[i];
        float component = point[i];

        // Ignore this axis if the component is between the boundaries
        if(start <= component && component <= end) continue;

        float d_to_end = component - end;
        d_to_end *= d_to_end;

        float d_to_start = component - start;
        d_to_start *= d_to_start;

        float diff = std::min(d_to_end, d_to_start);

        distance += diff;
    }

    return distance;
}
