#ifndef _TREE_H_
#define _TREE_H_

#include "vector.h"
#include "component.h"
#include "splitting.h"

Component * mktree(VectorList & points, Splitting & rule, int dimensions, int depth = 0);

#endif // _TREE_H_
