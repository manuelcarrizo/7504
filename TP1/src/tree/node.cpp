#include "node.h"

//#include <iomanip>

Node::Node(VectorList & points, float location, int axis, int depth,
           Component * left, Component * right)
    : Component(depth, this, left, right), location_(location), axis_(axis)
{
    VectorIterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        area_.update(*it);
    }
}

void Node::print(std::ostream & stream) const
{
    stream << "Node covering an area from " << area_.min()
           << " to " << area_.max() << ". Split at " << location_
           << " on axis " << axis_ << std::endl;
}

