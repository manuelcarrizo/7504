#ifndef _LEAF_H_
#define _LEAF_H_

#include "component.h"
#include "vector.h"

class Leaf : public Component
{
public:
    Leaf(VectorList & points, int depth, Component * left, Component * right);

    VectorList points();

    Vector * closest(Vector * other);
    Vector * closest(Vector & other);

    void print(std::ostream & stream) const;

private:
    VectorList points_;
};

#endif // _LEAF_H_
