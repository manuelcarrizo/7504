#ifndef _NODE_H_
#define _NODE_H_

#include "component.h"
#include "vector.h"
#include "area.h"

class Node : public Component
{
public:
    Node(VectorList & points, float location, int axis, int depth,
        Component * left, Component * right);

    void print(std::ostream & stream) const;

    float distance2(Vector * other);
    float distance2(Vector & other);

private:
    Area area_;
    float location_;
    int axis_;
};

#endif // _NODE_H_
