#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include <iostream>

/*
class Partition
{
public:
    Partition(float location, int depth, int axis)
        : location_(location), depth_(depth), axis_(axis) {}

    float location() const { return location_; }
    int depth() const { return depth_; }
    int axis() const { return axis_; }

    friend std::ostream& operator<<(std::ostream & stream, const Partition & p) {
        stream << "Partition at " << p.location() << ", depth " << p.depth()
            << ", axis " << p.axis() << std::endl;
        return stream;
    }

private:
    float location_;
    int depth_;
    int axis_;
};

*/

class Component
{
public:
    Component(int depth, Component * parent, Component * left, Component * right)
        : depth_(depth), parent_(parent), left_(left), right_(right) {}

    virtual ~Component() {}

    int depth() const { return depth_; }

    Component * parent() { return parent_; }
    Component * left() { return left_; }
    Component * right() { return right_; }

    Component * parent() const { return parent_; }
    Component * left() const { return left_; }
    Component * right() const { return right_; }

    virtual void print(std::ostream & stream) const = 0;

    friend std::ostream& operator<<(std::ostream & stream, const Component & c)
    {
        std::string fill(c.depth(), ' ');

        stream << fill;
        c.print(stream);

        if(c.left())
            stream << fill << "Left: " << std::endl << *c.left();

        if(c.right())
            stream << fill << "Right: " << std::endl << *c.right();

        return stream;
    }

private:
    int depth_;

    Component * parent_;
    Component * left_;
    Component * right_;
};

#endif // _COMPONENT_H_