#include <cmath>
#include <cstdlib>
#include <time.h>
#include <stdexcept>

#include "vector.h"

Vector::Vector(int dim)
{
    dimension_ = dim;
    coords_ = new float[dimension_];
}

Vector::Vector(const Vector & other)
{
    dimension_ = other.dimension_;
    coords_ = new float[dimension_];

    for(unsigned int i = 0 ; i < dimension_ ; i++) {
        coords_[i] = other.coords_[i];
    }
}

Vector::Vector(double x) : dimension_(1)
{
    coords_ = new float[dimension_];
    coords_[0] = x;
}

Vector::Vector(float x, float y) : dimension_(2)
{
    coords_ = new float[dimension_];
    coords_[0] = x;
    coords_[1] = y;
}

Vector::Vector(float x, float y, float z) : dimension_(3)
{
    coords_ = new float[dimension_];
    coords_[0] = x;
    coords_[1] = y;
    coords_[2] = z;
}

Vector::~Vector(){
    delete[] coords_;
}

float & Vector::operator[](unsigned int index)
{
    if(index < dimension_)
        return coords_[index];

    throw std::out_of_range("Index greater than vector size");
}

float Vector::distance(const Vector & other)
{
    return sqrt(square_distance(other));
}

float Vector::square_distance(const Vector & other)
{
    float ret = 0;
    for(unsigned int i = 0; i < dimension_ ; i++) {
        float diff = coords_[i] - other.coords_[i];

        ret += diff * diff;
    }
    return ret;
}

std::ostream& operator<< (std::ostream& stream, const Vector & vector)
{
    for(unsigned int i = 0; i < vector.dimension_ ; i++) {
        stream << vector.coords_[i] << ( i == (vector.dimension_ - 1) ? "" : " ");
    }

    return stream;
}

Vector * random_item(VectorList & list)
{
    static bool started = false;

    // only start the seed once
    if(!started) {
        srand (time(NULL));
        started = true;
    }

    int pos = rand() % list.size();
    
    VectorIterator it = list.begin();
    std::advance(it, pos);

    return *it;
}
