#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <iostream>
#include <list>

class Vector
{
private:
    unsigned int dimension_;
    float * coords_;

public:
    explicit Vector(int dim);
    Vector(const Vector & other);
    explicit Vector(double x);
    Vector(float x, float y);
    Vector(float x, float y, float z);
    ~Vector();

    int dimension() { return dimension_; };

    float & operator[](unsigned int index);

    float distance(const Vector & other);
    float square_distance(const Vector & other);

    friend std::ostream& operator<< (std::ostream& stream, const Vector & vector);
};

typedef std::list< Vector * > VectorList;
typedef VectorList::iterator VectorIterator;
typedef VectorList::const_iterator VectorConstIterator;

Vector * random_item(VectorList & list);

#endif // _VECTOR_H_
