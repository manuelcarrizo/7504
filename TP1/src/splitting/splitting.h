#ifndef _SPLITTING_H_
#define _SPLITTING_H_

#include "vector.h"

class Splitting
{
public:
    virtual ~Splitting() {};

    void axis(int value) { axis_ = value; }
    int axis() { return axis_; }

    virtual float value(VectorList & list) = 0;

private:
    int axis_ = 0;
};

#endif // _SPLITTING_H_
