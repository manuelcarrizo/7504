#ifndef _MEDIAN_H_
#define _MEDIAN_H_

#include "splitting.h"

class Median : public Splitting
{
public:
    Median();
    float value(VectorList & list);

private:
    float get_pivot(VectorList & list);

    float select(VectorList & list, int k);
    int partition(VectorList & list, float pivot, VectorList & left, VectorList & right);
};

#endif // _MEDIAN_H_
