#ifndef _AVERAGE_H_
#define _AVERAGE_H_

#include "splitting.h"

class Average : public Splitting
{
public:
    Average();
    float value(VectorList & list);
};

#endif // _AVERAGE_H_
