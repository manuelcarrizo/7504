#ifndef _HALF_H_
#define _HALF_H_

#include "splitting.h"

class Half : public Splitting
{
public:
    float value(VectorList & list);
};

#endif // _HALF_H_
