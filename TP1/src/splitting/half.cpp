#include <limits>

#include "half.h"

float Half::value(VectorList & list)
{
    float min = std::numeric_limits<float>::max(),
          max = 0;

    VectorIterator it;
    for(it = list.begin() ; it != list.end() ; it++) {
        float value = (**it)[axis()];

        if(value < min) {
            min = value;
        }

        if(value > max) {
            max = value;
        }
    }

    return (min + max) / 2;
}
