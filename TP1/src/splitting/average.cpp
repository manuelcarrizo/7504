#include <cstdlib>
#include <time.h>

#include "average.h"

static const int SAMPLES = 3;

Average::Average()
{
}

float Average::value(VectorList & list)
{
    int count = 0;
    float total = 0;

    if(list.size() > SAMPLES) {
        for(int i = 0 ; i < SAMPLES ; i++) {
            Vector * item = random_item(list);

            total += (*item)[axis()];
            count++;
        }
    }
    else {
        // Usar todos los valores si la lista tiene menos de SAMPLES elementos
        VectorIterator it;
        for(it = list.begin() ; it != list.end() ; it++) {
            total += (**it)[axis()];
            count++;
        }
    }

    return total / count;
}
