#include "median.h"

Median::Median()
{
}

float Median::value(VectorList & list)
{
    // perform quickselect to get the median
    return select(list, list.size() / 2);
}

float Median::get_pivot(VectorList & list)
{
    Vector * item = random_item(list);
    return (*item)[axis()];
}

float Median::select(VectorList & list, int k)
{
    float pivot = get_pivot(list);
    VectorList left, right;

    int position = partition(list, pivot, left, right);

    if(k == position) {
        return pivot;
    }
    else if(k < position) {
        return select(left, k);
    }
    else if(k > position) {
        return select(right, k - position);
    }

    return 0; // keep compiler happy
}

int Median::partition(VectorList & list, float pivot, VectorList & left, VectorList & right)
{
    VectorIterator it;
    for(it = list.begin() ; it != list.end() ; it++) {
        if((**it)[axis()] < pivot) {
            left.push_back(*it);
        }
        else {
            right.push_back(*it);
        }
    }
    return left.size();
}
