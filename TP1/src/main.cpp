#include <iostream>

#include "vector.h"
#include "area.h"

#include "average.h"
#include "half.h"
#include "median.h"

#include "tree/tree.h"

int main(int argc, char* argv[])
{
    VectorList l;

    /*
    l.push_back(new Vector(2, 8));
    l.push_back(new Vector(4, 1));
    l.push_back(new Vector(50, 3));
    l.push_back(new Vector(10, 10));
    l.push_back(new Vector(5, -5));
    */

    /*
    l.push_back(new Vector(82.3741, 29.3246));
    l.push_back(new Vector(99.7181, 79.2772));
    l.push_back(new Vector(23.2871, 67.9067));
    l.push_back(new Vector(18.9996, 72.0237));
    l.push_back(new Vector(57.6409, 18.0972));
    l.push_back(new Vector(18.6026, 35.1197));
    l.push_back(new Vector(62.0388, 77.7817));
    l.push_back(new Vector(3.03734, 5.92199));
    l.push_back(new Vector(66.6944, 89.8465));
    l.push_back(new Vector(76.0163, 33.5054));
    l.push_back(new Vector(21.9639, 85.7602));
    l.push_back(new Vector(79.3527, 17.4043));
    l.push_back(new Vector(92.3234, 53.3055));
    l.push_back(new Vector(62.1241, 94.3642));
    l.push_back(new Vector(11.678, 4.76252));
    l.push_back(new Vector(98.46, 28.9019));
    l.push_back(new Vector(63.7845, 47.7935));
    l.push_back(new Vector(71.5107, 26.3294));
    l.push_back(new Vector(88.1906, 64.7889));
    l.push_back(new Vector(34.9181, 71.0333));
    */

    /*
    l.push_back(new Vector(32.1463,40.2678));
    l.push_back(new Vector(15.3782,17.0479));
    l.push_back(new Vector(39.1065,41.5956));
    l.push_back(new Vector(11.9503,8.37869));
    l.push_back(new Vector(44.7849,27.9792));
    l.push_back(new Vector(4.67586,47.4434));
    l.push_back(new Vector(6.63167,29.7243));
    l.push_back(new Vector(47.7271,28.2125));
    l.push_back(new Vector(8.73146,13.2986));
    l.push_back(new Vector(40.0525,32.2437));
    */

    l.push_back(new Vector(46.6297));
    l.push_back(new Vector(13.9602));
    l.push_back(new Vector(4.42563));
    l.push_back(new Vector(41.754));
    l.push_back(new Vector(47.0822));
    l.push_back(new Vector(33.7357));
    l.push_back(new Vector(20.3421));
    l.push_back(new Vector(5.93341));
    l.push_back(new Vector(18.7973));
    l.push_back(new Vector(26.4253)); // << mediana
    l.push_back(new Vector(43.2074));
    l.push_back(new Vector(46.1685));
    l.push_back(new Vector(12.9364));
    l.push_back(new Vector(34.4949));
    l.push_back(new Vector(11.665));

    Average average;
    Half half;
    Median median;

    // std::cout << "Average: " << average.value(l) << std::endl;
    // std::cout << "Half: " << half.value(l) << std::endl;
    std::cout << "Median: " << median.value(l) << std::endl;

    // Component * kd = mktree(l, half, 2);

    // std::cout << *kd << std::endl;
    return 0;
}
